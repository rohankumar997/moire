use std::{
    cell::RefCell,
    path::PathBuf,
    rc::Rc,
    sync::{Arc, Mutex},
};

use aoide_desktop_app::settings;
use moire_ctrl::{circular_fifo, param::Registry};
use slint::{CloseRequestResponse, ComponentHandle};

use moire_audio_dsp::{engine as audio_engine, graph::RenderConfig, Seconds};
use moire_audio_io::jack::{self, JackBackend};

use moire_hid_io::ni_traktor_s4mk3;

pub use moire_library_backend::Backend as LibraryBackend;
pub use moire_library_frontend::{callback as library_callback, Frontend as LibraryFrontend};

use moire_ui_generated::MainWindow;

mod deck;
mod tasklet;

pub struct Gui {
    main_window: MainWindow,
    _tokio_rt: tokio::runtime::Handle,
    _library: Arc<Library>,
    _poll_audio_rx_timer: slint::Timer,
    _render_config: Rc<RefCell<RenderConfig>>,
    _to_audio_tx: Arc<Mutex<circular_fifo::Producer<audio_engine::Command>>>,
    _jack_backend: Rc<JackBackend>,
    _device_context: Rc<Option<RefCell<ni_traktor_s4mk3::DeviceContext>>>,
    _shared_param_registry: Arc<Mutex<Registry>>,
}

pub(crate) struct Library {
    backend: LibraryBackend,
    frontend: LibraryFrontend,
}

/// Gui is responsible for running the GUI after the rest of the
/// application has been initialized.
impl Gui {
    #[allow(clippy::too_many_arguments)] // FIXME
    pub fn new(
        config_dir: PathBuf,
        tokio_rt: tokio::runtime::Handle,
        library_backend: LibraryBackend,
        initial_settings: settings::State,
        initial_render_config: RenderConfig,
        to_audio_tx: circular_fifo::Producer<audio_engine::Command>,
        from_audio_rx: rtrb::Consumer<audio_engine::Event>,
        to_jack_tx: rtrb::Producer<jack::Command>,
        jack_backend: Rc<JackBackend>,
        gc_handle: basedrop::Handle,
        device_context: Option<ni_traktor_s4mk3::DeviceContext>,
    ) -> Gui {
        let main_window = MainWindow::new();

        let library_frontend =
            LibraryFrontend::spawn(config_dir, &tokio_rt, library_backend.handle());

        // Connect library backend properties (updated by tasklets) and callbacks

        tasklet::spawn_music_dir_changed_property_updater(
            main_window.as_weak(),
            &tokio_rt,
            &library_frontend,
        );
        main_window.on_lib_music_dir_choose(library_callback::on_music_dir_choose(
            tokio_rt.clone(),
            &library_frontend,
        ));
        main_window.on_lib_music_dir_reset(library_callback::on_music_dir_reset(&library_frontend));

        tasklet::spawn_collection_property_updater(
            main_window.as_weak(),
            &tokio_rt,
            &library_frontend,
        );
        main_window.on_lib_collection_rescan_music_dir(
            library_callback::on_collection_rescan_music_dir(
                tokio_rt.clone(),
                library_backend.handle().downgrade(),
                &library_frontend,
            ),
        );

        tasklet::spawn_track_search_property_updater(
            main_window.as_weak(),
            &tokio_rt,
            &library_frontend,
        );
        main_window.on_lib_track_search_submit_input({
            let mut on_track_search_submit_input =
                library_callback::on_track_search_submit_input(&library_frontend);
            move |input| on_track_search_submit_input(input.into()).into()
        });

        // Propagate the initial settings through the connected FRP network
        // after wiring everything together and before showing the UI.
        // This will also save the aoide settings persistently to disk.
        library_frontend.settings_state().modify(move |settings| {
            *settings = initial_settings;
            true
        });

        let library = Arc::new(Library {
            backend: library_backend,
            frontend: library_frontend,
        });

        let to_audio_tx = Arc::new(Mutex::new(to_audio_tx));
        let shared_param_registry = Arc::new(Mutex::new(Registry::default()));

        main_window.on_volume_changed_on_deck(deck::on_volume_changed(
            &to_audio_tx,
            &shared_param_registry,
        ));
        main_window
            .on_tempo_changed_on_deck(deck::on_tempo_changed(&to_audio_tx, &shared_param_registry));

        let render_config = Rc::new(RefCell::new(initial_render_config));
        let mut add_deck_callback = deck::on_add_deck(
            &main_window,
            &render_config,
            &to_audio_tx,
            to_jack_tx,
            &jack_backend,
            &gc_handle,
            &shared_param_registry,
        );
        // Add the first deck implicitly...
        add_deck_callback();
        // ...before attaching the callback to the UI.
        main_window.on_add_deck(add_deck_callback);

        let device_context = Rc::new(device_context.map(RefCell::new));
        {
            // TODO: on_close_requested() probably needs to do much more than just
            // finalizing and detaching the example device.
            let device_context = Rc::downgrade(&device_context);
            main_window.window().on_close_requested(move || {
                if let Some(device_context) = device_context.upgrade() {
                    if let Some(device_context) = &*device_context {
                        log::info!(
                            "Finalizing device: {device_info:?}",
                            device_info = device_context.borrow().info()
                        );
                        device_context.borrow_mut().finalize();
                    }
                }
                CloseRequestResponse::HideWindow
            })
        }

        main_window.on_load_file_to_deck(deck::on_load_file(
            &main_window,
            tokio_rt.clone(),
            &library,
            &render_config,
            &to_audio_tx,
            &gc_handle,
        ));

        main_window.on_clip_seek_on_deck(deck::on_clip_seek(&to_audio_tx));

        main_window.on_headphones_changed_on_deck(deck::on_headphones_changed(
            &to_audio_tx,
            &shared_param_registry,
        ));

        let poll_audio_rx_timer = slint::Timer::default();
        poll_audio_rx_timer.start(
            slint::TimerMode::Repeated,
            std::time::Duration::from_millis(100),
            on_gui_poll_timer(&main_window, &render_config, from_audio_rx),
        );

        Gui {
            main_window,
            _tokio_rt: tokio_rt,
            _library: library,
            _poll_audio_rx_timer: poll_audio_rx_timer,
            _render_config: render_config,
            _to_audio_tx: to_audio_tx,
            _jack_backend: jack_backend,
            _device_context: device_context,
            _shared_param_registry: shared_param_registry,
        }
    }

    pub fn run(&self) {
        self.main_window.run();
    }
}

fn on_gui_poll_timer(
    main_window: &MainWindow,
    render_config: &Rc<RefCell<RenderConfig>>,
    mut from_audio_rx: rtrb::Consumer<audio_engine::Event>,
) -> impl FnMut() {
    let main_window = main_window.as_weak();
    let render_config = Rc::downgrade(render_config);
    move || {
        let mut last_playhead_secs = None;
        while from_audio_rx.slots() > 0 {
            match from_audio_rx.pop().unwrap() {
                audio_engine::Event::EventsDropped(drop_count) => {
                    log::error!(
                        "Dropped {drop_count} events in realtime context that could not be emitted"
                    );
                }
                audio_engine::Event::PlayheadChanged(playhead_secs) => {
                    last_playhead_secs = Some(playhead_secs)
                }
                audio_engine::Event::RenderConfigChanged(new_render_config) => {
                    let render_config = render_config.upgrade().unwrap();
                    *render_config.borrow_mut() = new_render_config;
                }
            }
        }
        if let Some(playhead_secs) = last_playhead_secs {
            debug_assert!(playhead_secs >= Seconds::ZERO);
            let main_window = main_window.upgrade().unwrap();
            main_window.set_time(*playhead_secs as _);
            let hours = (*playhead_secs / 3600.0).floor();
            let minutes = (*playhead_secs / 60.0).floor();
            let seconds = (*playhead_secs % 60.0).floor();
            let deciseconds = ((*playhead_secs - playhead_secs.floor()) * 10.0).floor();
            let string = format!("{}:{}:{}.{}", hours, minutes, seconds, deciseconds);
            main_window.set_duration_text(string.into());
        }
    }
}

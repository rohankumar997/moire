use std::{future::Future, sync::Arc};

use discro::tasklet::OnChanged;
use slint::{ModelRc, VecModel, Weak};

use aoide_desktop_app::{collection, fs::DirPath, settings, track};

use moire_ui_generated as ui;

#[must_use]
fn music_dir_update_properties_in_event_loop(
    ui_handle: &Weak<ui::MainWindow>,
    music_dir: Option<&DirPath<'_>>,
) -> OnChanged {
    log::debug!("Updating music directory properties: {music_dir:?}");
    let music_dir_path = music_dir
        .map(|dir_path| dir_path.display().to_string())
        .unwrap_or_default();
    ui_handle
        .upgrade_in_event_loop({
            move |ui| {
                ui.set_lib_music_dir_is_valid(!music_dir_path.is_empty());
                ui.set_lib_music_dir_path(music_dir_path.into());
            }
        })
        .map(|()| OnChanged::Continue)
        .unwrap_or(OnChanged::Abort)
}

fn on_music_dir_changed_property_updater(
    ui_handle: Weak<ui::MainWindow>,
    library_frontend: &crate::LibraryFrontend,
) -> impl Future<Output = ()> + Send + 'static {
    let subscriber = library_frontend.settings_state().subscribe();
    settings::tasklet::on_music_dir_changed(subscriber, move |music_dir| {
        music_dir_update_properties_in_event_loop(&ui_handle, music_dir)
    })
}

pub(super) fn spawn_music_dir_changed_property_updater(
    ui_handle: Weak<ui::MainWindow>,
    tokio_rt: &tokio::runtime::Handle,
    library_frontend: &crate::LibraryFrontend,
) {
    tokio_rt.spawn(on_music_dir_changed_property_updater(
        ui_handle,
        library_frontend,
    ));
}

#[must_use]
fn u64_to_i32_saturating(value: u64) -> i32 {
    value.min(i32::MAX as u64) as i32
}

#[must_use]
fn collection_update_properties_in_event_loop(
    ui_handle: &Weak<ui::MainWindow>,
    state: &collection::State,
) -> OnChanged {
    log::debug!("Updating collection properties: {state:?}");
    let (is_idle, is_ready, entity_uid, tracks_count, playlists_count) = match state {
        collection::State::Ready(entity_with_summary) => {
            let entity_uid = entity_with_summary.entity.hdr.uid.to_string();
            let (tracks_count, playlists_count) =
                if let Some(summary) = &entity_with_summary.summary {
                    (
                        u64_to_i32_saturating(summary.tracks.total_count),
                        u64_to_i32_saturating(summary.playlists.total_count),
                    )
                } else {
                    (0, 0)
                };
            (
                state.is_idle(),
                true,
                entity_uid,
                tracks_count,
                playlists_count,
            )
        }
        _ => (
            state.is_idle(),
            false,
            state
                .entity_uid()
                .map(ToString::to_string)
                .unwrap_or_default(),
            0,
            0,
        ),
    };
    ui_handle
        .upgrade_in_event_loop({
            move |ui| {
                ui.set_lib_collection_is_idle(is_idle);
                ui.set_lib_collection_is_ready(is_ready);
                ui.set_lib_collection_uid(entity_uid.into());
                ui.set_lib_collection_tracks_count(tracks_count);
                ui.set_lib_collection_playlists_count(playlists_count);
            }
        })
        .map(|()| OnChanged::Continue)
        .unwrap_or(OnChanged::Abort)
}

fn on_collection_property_updater(
    ui_handle: Weak<ui::MainWindow>,
    library_frontend: &crate::LibraryFrontend,
) -> impl Future<Output = ()> + Send + 'static {
    let observable_state = Arc::clone(library_frontend.collection_state());
    let subscriber = observable_state.subscribe();
    collection::tasklet::on_state_tag_changed(subscriber, move |_| {
        collection_update_properties_in_event_loop(&ui_handle, &*observable_state.read())
    })
}

pub(super) fn spawn_collection_property_updater(
    ui_handle: Weak<ui::MainWindow>,
    tokio_rt: &tokio::runtime::Handle,
    library_frontend: &crate::LibraryFrontend,
) {
    tokio_rt.spawn(on_collection_property_updater(ui_handle, library_frontend));
}

#[must_use]
fn track_search_fetched_entity_to_ui(
    fetched_entity: &track::repo_search::FetchedEntity,
) -> ui::Track {
    let uid = fetched_entity.entity.hdr.uid.to_string().into();
    let artist = fetched_entity
        .entity
        .body
        .track
        .track_artist()
        .map_or_else(|| "Unknown Artist".into(), Into::into);
    let title = fetched_entity
        .entity
        .body
        .track
        .track_title()
        .map_or_else(|| "Unknown Title".into(), Into::into);
    ui::Track { uid, artist, title }
}

#[must_use]
fn track_search_update_properties_in_event_loop(
    ui_handle: &Weak<ui::MainWindow>,
    state: &track::repo_search::State,
) -> OnChanged {
    log::debug!("Updating track search properties");
    let is_enabled = state.context().collection_uid.is_some();
    let is_idle = state.is_idle();
    // TODO: Extend the model by adding tracks if possible instead of replacing it entirely.
    let tracks: Vec<_> = state
        .fetched_entities()
        .into_iter()
        .flatten()
        .map(track_search_fetched_entity_to_ui)
        .collect();

    ui_handle
        .upgrade_in_event_loop({
            move |ui| {
                ui.set_lib_track_search_is_enabled(is_enabled);
                ui.set_lib_track_search_is_idle(is_idle);
                ui.set_lib_track_search_model(ModelRc::new(VecModel::from(tracks)));
            }
        })
        .map(|()| OnChanged::Continue)
        .unwrap_or(OnChanged::Abort)
}

fn on_track_search_property_updater(
    ui_handle: Weak<ui::MainWindow>,
    library_frontend: &crate::LibraryFrontend,
) -> impl Future<Output = ()> + Send + 'static {
    let observable_state = Arc::clone(library_frontend.track_search_state());
    let subscriber = observable_state.subscribe();
    track::repo_search::tasklet::on_fetch_state_tag_changed(subscriber, move |_| {
        track_search_update_properties_in_event_loop(&ui_handle, &*observable_state.read())
    })
}

pub(super) fn spawn_track_search_property_updater(
    ui_handle: Weak<ui::MainWindow>,
    tokio_rt: &tokio::runtime::Handle,
    library_frontend: &crate::LibraryFrontend,
) {
    tokio_rt.spawn(on_track_search_property_updater(
        ui_handle,
        library_frontend,
    ));
}

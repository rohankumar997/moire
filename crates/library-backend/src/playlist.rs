use aoide_backend_embedded::playlist as api;

use aoide_core::{
    collection::EntityUid as CollectionUid,
    playlist::{Entity, EntityHeader, EntityUid, EntityWithEntries, Playlist},
};
use aoide_core_api::playlist::EntityWithEntriesSummary;

use crate::{Handle, Pagination, Result};

pub async fn load_one(handle: &Handle, entity_uid: EntityUid) -> Result<EntityWithEntries> {
    api::load_one(handle.db_gatekeeper(), entity_uid).await
}

pub async fn load_all(
    handle: &Handle,
    collection_uid: CollectionUid,
    kind: Option<String>,
    pagination: Option<Pagination>,
) -> Result<Vec<EntityWithEntriesSummary>> {
    api::load_all(handle.db_gatekeeper(), collection_uid, kind, pagination).await
}

pub async fn create(
    handle: &Handle,
    collection_uid: CollectionUid,
    new_playlist: Playlist,
) -> Result<Entity> {
    api::create(handle.db_gatekeeper(), collection_uid, new_playlist).await
}

pub async fn update(
    handle: &Handle,
    entity_header: EntityHeader,
    modified_playlist: Playlist,
) -> Result<Entity> {
    api::update(handle.db_gatekeeper(), entity_header, modified_playlist).await
}

pub async fn purge(handle: &Handle, entity_uid: EntityUid) -> Result<()> {
    api::purge(handle.db_gatekeeper(), entity_uid).await
}

pub async fn patch_entries(
    handle: &Handle,
    entity_header: EntityHeader,
    operations: impl IntoIterator<Item = aoide_usecases_sqlite::playlist::entries::PatchOperation>
        + Send
        + 'static,
) -> Result<EntityWithEntriesSummary> {
    api::entries::patch(handle.db_gatekeeper(), entity_header, operations).await
}

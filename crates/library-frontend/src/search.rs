//! Searching the library

use aoide_core::track::tag::{FACET_COMMENT, FACET_GENRE, FACET_GROUPING, FACET_MOOD};
use aoide_core_api::{
    filtering::StringPredicate,
    track::search::{PhraseFieldFilter, StringField},
};

pub(crate) fn parse_track_search_filter_from_input(
    input: &str,
) -> Option<aoide_core_api::track::search::Filter> {
    debug_assert_eq!(input, input.trim());
    if input.is_empty() {
        return None;
    }
    let phrase_fields = [StringField::Publisher];
    let tag_facets = [
        FACET_COMMENT.to_owned(),
        FACET_GROUPING.to_owned(),
        FACET_GENRE.to_owned(),
        FACET_MOOD.to_owned(),
    ];
    // The size of the filter and as a consequence the execution time
    // scales linearly with the number of terms in the input.
    let all_filters: Vec<_> = input
        .split_whitespace()
        .map(|term| {
            let title_phrase = aoide_core_api::track::search::Filter::TitlePhrase(
                aoide_core_api::track::search::TitlePhraseFilter {
                    name_terms: vec![term.to_owned()],
                    ..Default::default()
                },
            );
            let actor_phrase = aoide_core_api::track::search::Filter::ActorPhrase(
                aoide_core_api::track::search::ActorPhraseFilter {
                    name_terms: vec![term.to_owned()],
                    ..Default::default()
                },
            );
            let field_phrase = aoide_core_api::track::search::Filter::Phrase(PhraseFieldFilter {
                fields: phrase_fields.to_vec(),
                terms: vec![term.to_owned()],
            });
            let tag =
                aoide_core_api::track::search::Filter::Tag(aoide_core_api::tag::search::Filter {
                    facets: Some(tag_facets.to_vec()),
                    label: Some(StringPredicate::Contains(term.to_owned())),
                    ..Default::default()
                });
            aoide_core_api::track::search::Filter::Any(vec![
                title_phrase,
                actor_phrase,
                field_phrase,
                tag,
            ])
        })
        .collect();
    debug_assert!(!all_filters.is_empty());
    Some(aoide_core_api::track::search::Filter::All(all_filters))
}

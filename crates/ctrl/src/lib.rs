#![warn(rust_2018_idioms)]
#![warn(rust_2021_compatibility)]
#![warn(missing_debug_implementations)]
#![warn(unreachable_pub)]
#![warn(unsafe_code)]
#![warn(clippy::pedantic)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::missing_errors_doc)]
#![warn(rustdoc::broken_intra_doc_links)]

pub mod atomic;
pub mod circular_fifo;
pub mod interpolation;
pub mod param;

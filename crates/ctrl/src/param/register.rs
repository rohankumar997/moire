use std::{
    borrow::Borrow,
    collections::{hash_map::Entry, HashMap},
    sync::Arc,
};

use atomic::AtomicValue;
use thiserror::Error;

use crate::param::Direction;

use super::{atomic, Address, Descriptor};

const INITIAL_CAPACITY: usize = 1024;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct Id(u32);

impl Id {
    #[must_use]
    pub fn to_value(self) -> u32 {
        let Self(value) = self;
        value
    }
}

#[derive(Debug, Clone)]
pub struct Param<'a> {
    pub address: Address<'a>,
    pub descriptor: Descriptor,
    pub id: Id,
}

/// Map address to consecutive ids.
#[allow(missing_debug_implementations)]
struct AddressToIdMap {
    inner: HashMap<Address<'static>, Id>,
}

impl AddressToIdMap {
    #[must_use]
    fn with_capacity(initial_capacity: usize) -> Self {
        Self {
            inner: HashMap::with_capacity(initial_capacity),
        }
    }

    fn len(&self) -> usize {
        self.inner.len()
    }

    fn iter(&self) -> impl Iterator<Item = (&Address<'static>, Id)> {
        self.inner.iter().map(|(address, &id)| (address, id))
    }

    /// Obtain an id for an address.
    fn get_or_add(&mut self, address: Address<'static>) -> (Address<'static>, Id) {
        // The current length must be obtained before the mutable borrow,
        // even if it remains unused.
        let next_id = self.len();
        match self.inner.entry(address) {
            Entry::Occupied(entry) => {
                let id = *entry.get();
                // TODO: Replace needless clone() with entry.replace_key()
                // after #![feature(map_entry_replace)] has been stabilized.
                //let address = entry.replace_key();
                let address = entry.key().clone();
                (address, id)
            }
            Entry::Vacant(entry) => {
                let id = Id(u32::try_from(next_id).expect("id overflow"));
                let address = entry.key().clone();
                entry.insert(id);
                (address, id)
            }
        }
    }

    fn get(&self, address: &impl Borrow<str>) -> Option<Id> {
        self.inner.get(address.borrow()).map(ToOwned::to_owned)
    }
}

struct RegisteredEntry<'a> {
    address: Address<'a>,
    descriptor: Option<Descriptor>,
    output_value: Option<atomic::SharedValue>,
}

#[derive(Debug, Error)]
pub enum RegisterError {
    /// The address is already in use and the descriptors differ.
    ///
    /// Could only occur when registering a provider.
    #[error("address occupied")]
    AddressOccupied,
}

/// Parameter registry.
///
/// Permanently maps addresses to ids and stores metadata
/// about the associated parameters.
#[allow(missing_debug_implementations)]
pub struct Registry {
    address_to_id: AddressToIdMap,
    entries: Vec<RegisteredEntry<'static>>,
}

fn registry_entry_id(id: Id) -> usize {
    // This cast is safe, because the range is checked upon registration.
    id.to_value() as usize
}

impl Registry {
    pub fn address_to_id_iter(&self) -> impl Iterator<Item = (&Address<'static>, Id)> {
        self.address_to_id.iter()
    }

    fn register(&mut self, address: Address<'static>) -> (Id, &mut RegisteredEntry<'static>) {
        debug_assert_eq!(self.address_to_id.len(), self.entries.len());
        let (address, id) = self.address_to_id.get_or_add(address);
        let entry_id = registry_entry_id(id);
        if entry_id < self.entries.len() {
            // Occupied
            debug_assert_eq!(self.address_to_id.len(), self.entries.len());
            #[allow(unsafe_code)]
            let entry = unsafe { self.entries.get_unchecked_mut(id.to_value() as usize) };
            (id, entry)
        } else {
            // Vacant
            let new_entry = RegisteredEntry {
                address,
                descriptor: None,
                output_value: None,
            };
            self.entries.push(new_entry);
            debug_assert_eq!(self.address_to_id.len(), self.entries.len());
            let entry = self.entries
                .last_mut()
                // Safe unwrap after push
                .unwrap();
            (id, entry)
        }
    }

    /// Register the parameter descriptor for an address.
    ///
    /// Re-registering the same parameter twice registers only a single parameter
    /// if the descriptors match. If the descriptors do not match, a [`RegisterError`]
    /// is returned.
    ///
    /// For output parameters registering a descriptor adds a shared, atomic
    /// value that is initialized with the default parameter value. The
    /// registry will keep a strong reference to this shared value and
    /// provide it together with the descriptor.
    ///
    /// Addresses strings will be used verbatim as the key.
    #[allow(clippy::missing_panics_doc)]
    pub fn register_descriptor(
        &mut self,
        address: Address<'static>,
        descriptor: Descriptor,
    ) -> Result<
        (
            Id,
            &Address<'static>,
            &Descriptor,
            Option<&atomic::SharedValue>,
        ),
        RegisterError,
    > {
        let (id, entry) = self.register(address);
        let RegisteredEntry {
            address,
            descriptor: registered_descriptor,
            output_value: registered_output_value,
        } = entry;
        let descriptor = if let Some(registered_descriptor) = registered_descriptor {
            if registered_descriptor != &descriptor {
                return Err(RegisterError::AddressOccupied);
            }
            log::debug!("Descriptor already registered @ {address}: {descriptor:?}");
            registered_descriptor
        } else {
            log::debug!("Registering descriptor @ {address}: {descriptor:?}");
            debug_assert!(registered_output_value.is_none());
            let output_value = match descriptor.direction {
                Direction::Input => None,
                Direction::Output => Some(Arc::new(AtomicValue::from(descriptor.value.default))),
            };
            *registered_descriptor = Some(descriptor);
            *registered_output_value = output_value;
            // Safe unwrap (see above)
            registered_descriptor.as_ref().unwrap()
        };
        Ok((id, address, descriptor, registered_output_value.as_ref()))
    }

    /// Register a parameter address.
    ///
    /// Addresses can be registered at any time, even before the corresponding descriptor
    /// is registered. The descriptor will not be available until it has been registered.
    pub fn register_address(
        &mut self,
        address: Address<'static>,
    ) -> (
        Id,
        &Address<'static>,
        Option<&Descriptor>,
        Option<&atomic::SharedValue>,
    ) {
        let (id, entry) = self.register(address);
        let RegisteredEntry {
            address,
            descriptor,
            output_value,
        } = entry;
        (id, address, descriptor.as_ref(), output_value.as_ref())
    }

    /// Get the metadata of a parameter by id.
    #[must_use]
    pub fn get_registered(
        &self,
        id: Id,
    ) -> Option<(
        &Address<'static>,
        Option<&Descriptor>,
        Option<&atomic::SharedValue>,
    )> {
        self.entries.get(registry_entry_id(id)).map(|entry| {
            let RegisteredEntry {
                address,
                descriptor,
                output_value,
            } = entry;
            (address, descriptor.as_ref(), output_value.as_ref())
        })
    }

    /// Find the metadata of a parameter by address.
    #[must_use]
    pub fn find_registered(
        &self,
        address: &impl Borrow<str>,
    ) -> Option<(Id, Option<&Descriptor>, Option<&atomic::SharedValue>)> {
        self.address_to_id
            .get(address)
            .and_then(|id| {
                self.entries
                    .get(registry_entry_id(id))
                    .map(|entry| (id, entry))
            })
            .map(|(id, entry)| {
                let RegisteredEntry {
                    address: entry_address,
                    descriptor,
                    output_value,
                } = entry;
                debug_assert_eq!(address.borrow(), entry_address);
                (id, descriptor.as_ref(), output_value.as_ref())
            })
    }
}

impl Default for Registry {
    fn default() -> Self {
        Self {
            // Reserve some extra space in the underlying `HashMap` to reduce collisions
            address_to_id: AddressToIdMap::with_capacity(INITIAL_CAPACITY + INITIAL_CAPACITY / 2),
            entries: Vec::with_capacity(INITIAL_CAPACITY),
        }
    }
}

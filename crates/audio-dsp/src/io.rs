use crate::ISampleRate;

pub enum Event {
    SampleRateChanged(ISampleRate),
}

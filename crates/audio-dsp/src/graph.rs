//! Audio rendering graph

use thiserror::Error;

use moire_ctrl::param::{self, Param};

use crate::{FrameSize, IFrames, SampleRate};

/// Layout of the input/output channel buffers within the chunk buffer
/// that is passed to [`Node::render()`].
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ChannelBufferLayout {
    /// In-place rendering, i.e. output channels overwrite input channels.
    ///
    /// The number of channel buffers equals the maximum of input and
    /// output channels.
    Overlay,

    /// Immutable input channels, followed by mutable output channels.
    ///
    /// The number of channel buffers equals the sum of input and
    /// output channels.
    ///
    /// Modifying the contents of the immutable input channel buffers
    /// will cause undefined behavior!
    Disjunct,
}

#[derive(Debug, Clone, PartialEq)]
pub struct RenderConfig {
    /// Frames per second
    pub sample_rate: SampleRate,

    /// Number of sample frames
    pub samples_per_channel: FrameSize,

    /// Arrangement of input/output channel buffers
    pub channel_buffer_layout: ChannelBufferLayout,
}

impl RenderConfig {
    #[must_use]
    pub fn is_valid(&self) -> bool {
        let Self {
            sample_rate,
            samples_per_channel,
            channel_buffer_layout: _,
        } = self;
        *samples_per_channel > FrameSize::ZERO && *sample_rate > SampleRate::ZERO
    }
}

#[derive(Debug, Clone, Default)]
pub struct NodeDescriptor {
    /// Input parameters for real-time control.
    ///
    /// The position of the parameters in this slice defines the local parameter
    /// index that is then passed as an argument to [`Node::set_input_param_value()`]
    /// for setting the input parameters.
    pub params_in: Vec<Param<'static>>,

    /// Output parameters for monitoring real-time state (invariant).
    pub params_out: Vec<(Param<'static>, param::atomic::SharedValue)>,

    /// Number of input channels.
    pub channels_in: usize,

    /// Number of output channels.
    pub channels_out: usize,

    /// Restrict the channel buffer layout.
    ///
    /// When set to `None` the node will be informed about the effective
    /// [`ChannelBufferLayout`] during [`Node::prepare_to_render()`] and
    /// needs to adapt its rendering accordingly. This is the default behavior.
    ///
    /// Setting it to `Some` guarantees that the input/output channel buffers
    /// are always laid out as requested. Depending on the circumstances
    /// this might require additional copying of buffers during rendering.
    pub channel_buffer_layout: Option<ChannelBufferLayout>,

    /// Minimum number of sample frames for [`Node::render()`].
    pub min_samples_per_channel: Option<FrameSize>,

    /// Maximum number of sample frames for [`Node::render()`].
    pub max_samples_per_channel: Option<FrameSize>,
}

#[derive(Debug, Error)]
pub enum InitializeError {
    #[error(transparent)]
    RegisterParam(#[from] param::RegisterError),

    #[error(transparent)]
    Anyhow(#[from] anyhow::Error),
}

#[derive(Debug, Error)]
pub enum PrepareToRenderError {
    #[error("unsupported config")]
    UnsupportedConfig,

    #[error(transparent)]
    Anyhow(#[from] anyhow::Error),
}

/// Dynamic properties that depend on the [`RenderConfig`].
#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub struct RenderProps {
    /// The expected latency in frames (>= 0) for latency compensation.
    pub latency: IFrames,
}

/// Node in an audio rendering graph.
pub trait Node<S> {
    /// Initialize the node.
    ///
    /// This method is supposed to be invoked only once to perform the
    /// setup, e.g. registering of input/output parameters.
    ///
    /// Returns the invariant descriptor of this node.
    fn initialize(
        &mut self,
        param_registry: &mut param::Registry,
    ) -> Result<NodeDescriptor, InitializeError>;

    /// Set the value of an input parameter.
    ///
    /// The `index` matches the position of the corresponding input
    /// parameter in [`NodeDescriptor::params_in`].
    ///
    /// This method could be invoked at any time after [`Node::initialize()`]
    /// and must be real-time safe.
    fn set_input_param_value(&mut self, index: usize, value: param::Value);

    /// Prepare to render before entering the real-time context.
    ///
    /// This method might be invoked repeatedly to abort and reconfigure
    /// the rendering.
    ///
    /// `first_frame` denotes the position on the timeline of the outer context.
    /// Every invocation of [`Node::render()`] is supposed to implicitly advance
    /// the position by [`RenderConfig::samples_per_channel`] sample frames.
    ///
    /// This method does not need to be real-time safe, e.g. it might
    /// (de-)allocate memory for buffering or may block for acquiring
    /// or releasing other resources.
    fn prepare_to_render(
        &mut self,
        render_config: &RenderConfig,
        first_frame: IFrames,
    ) -> Result<RenderProps, PrepareToRenderError>;

    /// Render a chunk of audio.
    ///
    /// Invoked repeatedly after [`Self::prepare_to_render()`] on a contiguous
    /// range of frames. The step size defined by [`RenderConfig::samples_per_channel`].
    ///
    /// `render_buf`: Combined input/output buffer with a chunk of audio data.
    /// The length of each channel buffer (e.g. `render_buf[0].len()`) equals
    /// `RenderConfig::samples_per_channel` and the whole output buffer must
    /// be filled with samples.
    ///
    /// See [`Self::debug_assert_render_preconditions()`] for various assumptions
    /// and preconditions.
    fn render<ChannelBuf: AsMut<[S]>>(&mut self, render_buf: &mut [ChannelBuf]);

    /// Utility function to verify the consistency between configuration
    /// and invocation during development.
    #[cfg(debug_assertions)]
    fn debug_assert_render_preconditions<ChannelBuf: AsMut<[S]>>(
        &self,
        node_descriptor: &NodeDescriptor,
        render_config: &RenderConfig,
        render_buf: &mut [ChannelBuf],
    ) {
        debug_assert!(render_config.is_valid());
        let RenderConfig {
            sample_rate: _,
            samples_per_channel,
            channel_buffer_layout,
        } = render_config;
        debug_assert_eq!(
            node_descriptor
                .channel_buffer_layout
                .unwrap_or(*channel_buffer_layout),
            *channel_buffer_layout
        );
        debug_assert!(!render_buf.is_empty());
        match channel_buffer_layout {
            ChannelBufferLayout::Overlay => {
                debug_assert_eq!(
                    render_buf.len(),
                    node_descriptor
                        .channels_in
                        .max(node_descriptor.channels_out)
                );
            }
            ChannelBufferLayout::Disjunct => {
                debug_assert_eq!(
                    render_buf.len(),
                    node_descriptor.channels_in + node_descriptor.channels_out
                );
            }
        }
        for channel_buf in render_buf {
            debug_assert_eq!(channel_buf.as_mut().len(), **samples_per_channel);
        }
    }

    #[cfg(not(debug_assertions))]
    #[inline]
    fn debug_assert_render_preconditions<ChannelBuf: AsMut<[S]>>(
        &self,
        _: &NodeDescriptor,
        _: &RenderConfig,
        _: &mut [ChannelBuf],
    ) {
    }
}
